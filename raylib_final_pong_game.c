/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO = 0, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    //
     int screenWidth = 800;
     int screenHeight = 450;
     char windowTitle[30] = "Final Pong";
     
     GameScreen screen = LOGO;
    
     // TODO: Define required variables here..........................(0.5p)
     // NOTE: Here there are some useful variables (should be initialized)
     Rectangle player;
     player.width = 15;
     player.height = 70;
     player.x = 20;
     player.y = screenHeight/2 - player.height/2;
     int playerSpeedY = 10;
    
     Rectangle enemy;
     enemy.width = 15;
     enemy.height = 70;
     enemy.x = screenWidth-40;
     enemy.y = screenHeight/2 - enemy.height/2;
     int enemySpeedY = 9.5;
     
     Rectangle lineV;
     lineV.width = 6;
     lineV.height = screenHeight;
     lineV.x = screenWidth/2-lineV.width/2;
     lineV.y = 50;
     
     Rectangle lineH;
     lineH.width = screenWidth;
     lineH.height = 6;
     lineH.x = 0;
     lineH.y = 50;
     
     
     Vector2 ballPosition = {screenWidth/2, screenHeight/2};
     Vector2 ballSpeed;
     float speed = 4;
     ballSpeed.x = -speed;
     ballSpeed.y = -speed;
    
     int ballRadius = 10;
    
     Rectangle playerLife;
     playerLife.width = 200;
     playerLife.height = 30;
     playerLife.x = 10;
     playerLife.y = 10;
     
     Rectangle enemyLife;
     enemyLife.width = 200;
     enemyLife.height = 30;
     enemyLife.x = screenWidth - enemyLife.width - 10;
     enemyLife.y = 10;

     Rectangle playerBackground = {5, 5, 210, 40};
     Rectangle enemyBackground = {screenWidth - 210 - 5, 5, 210, 40};

     int damage = 15;
     bool pause = false;

     //bars
    
     int secondsCounter = 99;
    
     int framesCounter = 0;         // General pourpose frames counter
    
     int gameResult = -1;     // 0 - Loose, 1 - Win, -1 - Not defined
    
     InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
     bool fadeIn = true;
     
     Image image = LoadImage("logotaiworks.png");
     Texture2D logos = LoadTextureFromImage(image);
     Color colorlogo = BLACK;
     colorlogo.a = 0;
     
     Image image2 = LoadImage("pongtitles.png");
     Texture2D titles = LoadTextureFromImage(image2);
     Color colortitle = WHITE;
     colortitle.a = 0;
     
     int pressStartCounter = 0;

    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
     Sound soundfx = LoadSound("sound.wav");
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                 framesCounter++;
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                 if(fadeIn){
                 if(colorlogo.a < 255){
                     colorlogo.a++;
                    }else {
                     framesCounter++;
                     if (framesCounter > 180){
                         fadeIn = false;
                        }
                    }
                }else {
                     if (colorlogo.a > 0){
                         colorlogo.a--;
                        } else {
                        screen = TITLE;
                    }
                }
                }
             break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                 
                // TODO: Title animation logic.......................(0.5p)
                 fadeIn = true;
                 framesCounter++;
                     if(fadeIn){
                         if(colortitle.a < 255){
                         colortitle.a++;
                         if (framesCounter >= 60){
                             pressStartCounter++;
                             framesCounter = 0;
                            }
                        }
                     }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                 if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
             
                // TODO: Ball movement logic.........................(0.2p)
                
                 ballPosition.x = ballPosition.x + ballSpeed.x;
                 ballPosition.y = ballPosition.y + ballSpeed.y;
             
                // TODO: Player movement logic.......................(0.2p)
                
                 if (IsKeyDown(KEY_UP)) player.y -= playerSpeedY;
                 if (IsKeyDown(KEY_DOWN)) player.y += playerSpeedY;
                 if (player.y <= 50){
                     player.y = 50;
                 }
                 if (player.y + player.height >= screenHeight){
                     player.y = screenHeight - player.height;
                 }
                // TODO: Enemy movement logic (IA)...................(1p)
                
                 if (ballPosition.x > screenWidth/2){
                     if (ballPosition.y < enemy.y + enemy.height/2){
                         enemy.y -= enemySpeedY;
                    }else if (ballPosition.y > enemy.y + enemy.height/2){
                         enemy.y += enemySpeedY;
                    }
                }
                 if (enemy.y <= 50){
                     enemy.y = 50;
                    }
                 if (enemy.y + enemy.height >= screenHeight){
                     enemy.y = screenHeight - enemy.height;
                    }
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                 if(CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                     ballSpeed.x *= -1.025;      
                     PlaySound(soundfx);
                }  
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                 if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                     ballSpeed.x *= -1.025;      
                     PlaySound(soundfx);
                }  
                // TODO: Collision detection (ball-limits) logic.....(1p)
               
                 if(ballPosition.y + ballRadius > screenHeight){  
                     ballSpeed.y *= -1;
                    }
                 if (ballPosition.y - ballRadius <= 50 ){
                     ballSpeed.y *= -1;
                    }
  
                // TODO: Life bars decrease logic....................(1p)
                    //horizontal limits + life bars decrease
                      if(ballPosition.x + ballRadius >= screenWidth){
                         ballSpeed.x *= -1;
                         enemyLife.width -= damage;
                         
                        }
                     if (ballPosition.x - ballRadius <= 0){
                         ballSpeed.x *= -1;
                         playerLife.width -= damage;
                        }

                // TODO: Time counter logic..........................(0.2p)
                        framesCounter++;
                         if(framesCounter >= 60){
                             secondsCounter--;
                             framesCounter = 0;
                            }

                // TODO: Game ending logic...........................(0.2p)
                    if (playerLife.width <= 0){
                         screen = ENDING;
                         gameResult = 0;
                        }
                     if (enemyLife.width <= 0){
                         screen = ENDING;
                         gameResult = 1;
                        }
                     if (secondsCounter <= 0){
                         screen = ENDING;
                         gameResult = -1;
                        }

                // TODO: Pause button logic..........................(0.2p)
   
                 if (IsKeyPressed(KEY_P)){
                     pause = !pause;
                    }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
           
                // TODO: Replay / Exit game logic....................(0.5p)
                 if (IsKeyPressed(KEY_ENTER)){
                 screen = GAMEPLAY;
                 playerLife.width = 200;
                 enemyLife.width = 200;
                 secondsCounter = 99;
                 framesCounter = 0;
                 ballPosition.x = screenWidth/2;
                 ballPosition.y = screenHeight/2;
                 ballSpeed.x = 4;
                 ballSpeed.y = 4;
                 }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                     
                    // TODO: Draw Logo...............................(0.2p)
                     ClearBackground(WHITE);
                     DrawTexture(logos, screenWidth/2 - logos.width/2, screenHeight/2 - logos.height/2, colorlogo);
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                     ClearBackground(WHITE);
                    // TODO: Draw Title..............................(0.2p)
                     
                     DrawTexture(titles, 0, 0, colortitle);

                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (pressStartCounter % 2 != 0) DrawText("Press Enter!",screenWidth/2 - MeasureText("Press Enter!", 25)/2, screenHeight/1.75, 25, GRAY);

                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                 
                    // TODO: Draw player and enemy...................(0.2p)
                     ClearBackground(RAYWHITE);
                     DrawRectangleRec(player, PURPLE);
                     DrawRectangleRec(enemy, BLUE);
                     DrawRectangleRec(lineV, BLACK);
                     DrawRectangleRec(lineH, BLACK);
                     DrawCircleV(ballPosition, ballRadius, MAROON);
    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                     DrawRectangleRec(playerBackground, BLACK);
                     DrawRectangleRec(enemyBackground, BLACK);
                     DrawRectangleRec(playerLife, PURPLE);
                     DrawRectangleRec(enemyLife, BLUE);

                    // TODO: Draw time counter.......................(0.5p)
                     DrawText(FormatText("%d", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, 10, 40, RED);
                    // TODO: Draw pause message when required........(0.5p)
                      if(pause == true){
                         DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 50)/2, screenHeight/2, 50, RED);
                        }

                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                     
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (gameResult == 1){
                         DrawText("CONGRATULATIONS, YOU WON", screenWidth/3 - MeasureText("CONGRATULATIONS, YOU WON", 50)/4, screenHeight/2 - 40, 40, GREEN);
                    } else if (gameResult == 0){
                         DrawText("TOO BAD", screenWidth/2 - MeasureText("TOO BAD", 50)/2, screenHeight/2 - 40, 40, RED);
                    } else {
                         DrawText("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME", 50)/2, screenHeight/2 - 40, 40, GRAY);
                        }
                     DrawText("Press Enter to try again!",screenWidth/2 - MeasureText("Press Enter to try again", 30)/2, screenHeight/3*2, 30, PURPLE );
                     DrawText("Press Esc to close!",screenWidth/2 - MeasureText("Press Esc to close", 30)/2, screenHeight/3.5*2, 30, PURPLE );

                } break;
                default: break;
            }
        
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
     UnloadTexture(logos);
     UnloadTexture(titles);
     UnloadSound(soundfx);
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}